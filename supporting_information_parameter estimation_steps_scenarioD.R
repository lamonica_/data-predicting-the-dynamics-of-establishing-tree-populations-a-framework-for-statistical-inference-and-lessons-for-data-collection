
##########################################################
########### PARAMETER ESTIMATION FOR D SCENARIO ##########
##########################################################
rm(list = ls())
load("example_data_and_prior.RData") 
source("supporting_information_model_code.R")

#STEP 1: likelihood based inference of dendrochronological data

########################size and growth increment data preparation###################################
#the list "data" is an example data of one forest simulation
#list of three elements: 
#[[1]] is a dataframe in which rows correspond to the individuals in the population, four columns: id is the cell number where the tree stands, birth_year is the year of birth, mother and father are the cell number of the individual mother and father trees 
#[[2]] is the matrix of sizes of the individuals (rows -> individuals, columns -> years, starting year 0)
#[[3]] is the matrix of growth increments of the individuals (rows -> individuals, columns -> years, starting year 1)
Pedigree=data[[1]][data[[2]][,Nyear+1]>=3,] #removing individuals smaller than 3cm dbh
Pedigree$birth_year[1]=1
Size=data[[2]]data[[2]][data[[2]][,Nyear+1]>=3,] #removing individuals smaller than 3cm dbh
Growth=data[[3]]data[[2]][data[[2]][,Nyear+1]>=3,] #removing individuals smaller than 3cm dbh

library(raster)

grid=f_grid(nb_cell=3025, patch_size=55)

Nyear=70
N_tree=length(Pedigree[,1]) #number of individuals
Age=71-Pedigree$birth_year

#reordering size and growth increment matrices to have the column starting from year 1 to Age of the individual and zeros after, for JAGS code
Size_jags=matrix(NA,nrow=N_tree, ncol=71)
Size_jags[1,]=Size[1,]
for (i in 2:N_tree){
  vect=Size[i,-(1:(Pedigree$birth_year[i]))] 
  Size_jags[i,1:(Age[i])]=vect
}

Growth_jags=matrix(NA,nrow=N_tree, ncol=70)
Growth_jags[1,]=Growth[1,]
for (i in 2:N_tree){
  vect=Growth[i,-(1:(Pedigree$birth_year[i]))]
  Growth_jags[i,1:(Age[i]-1)]=vect
}


###############distance between individuals matrix#############################
D_full <- dist(coordinates(grid), method = "euclidean", diag=T)
D_full <- as.matrix(D_full)

##matrix with only the cells actually occupied by individuals
cell_occ=rep(0, length(grid))
cell_occ[Pedigree$id]=1
D_occ=D_full[cell_occ==1,cell_occ==1]

##corresponding index matrix and index pedigree
index_row=as.numeric()
for (i in 1:N_tree){
  index_row[i]=which(colnames(D_occ)==Pedigree$id[i])
}

##reordering the matrix
D_order=matrix(0,nrow=N_tree, ncol=N_tree)
for (i in 1:nbtree){
  D_order[i,]=D_occ[index_row[i],] 
}
D=matrix(0,nrow=N_tree, ncol=N_tree)
for (i in 1:N_tree){
  D[,i]=D_order[,index_row[i]] 
}

##neighbours matrix to remove self competition, and in case there is a distance threshold for competition
competition_threshold=15
Neighbours <- apply(Mdist, c(1,2), function (x) {ifelse(x<competition_threshold,1,0)})
diag(Neighbours) <- 0


##############likelihood based parameter estimation with JAGS###############################################
library(snow)
library(dclone)
library(rjags)

##model
model_code <- "
{
 
  for (i in 1:N_tree){ 
 
    for (j in 1:N_tree){
    kernel[i,j] <- exp(-(D[i,j]^2)/b2^2)*Neighbours[i,j] #mdist = D
    }
    
    for (n in 1:Age[i]){
    y_c[i,n] <- sum(kernel[i,1:N_tree]*X[1:N_tree,(Nyear-Age[i]+n)] )
    mu_g[i,n]<-(a1*exp(-0.5*(log(x[i,n]/a2)/a3)^2))*exp(-b1*y_c[i,n])
    y_g[i,n] ~ dnorm(mu_g[i,n],tau) 
    }#n
   
  }#i

##priors  
a1 ~ dunif(1,5)
a2 ~ dunif(1.2,30)
a3 ~ dunif(0.8,2)

log10a4 ~ dunif(-2,0)
a4 <- pow(10,log10a4)
tau <- 1/(pow(a4,2))

log10b1 ~ dunif(-2.7,-0.7)
b1 <- pow(10,log10b1)

b2 ~ dunif(1,20)
}
"
data <- list(N_tree=N_tree, Age=Age, Nyear=Nyear,
             Mdist=Mdist, Neighbours=Neighbours,
             x=Size_jags, X=Size, y_g=Growth_jags
)


cluster <- makePSOCKcluster(3)

parJagsModel(cluster, name = "run", file = textConnection(model_code),
             data = data,
             n.chains=3)
parameters <- c("a1","a2","a3","log10a4","log10b1","b2")
mcmc <- parCodaSamples(cluster,"run", parameters, n.iter =10000, thin =1)

stopCluster(cluster)


#STEP 2: generate a matrix of priors (columns: parameters) from step one for growth and competition parameters and from the priors listed in Table 2 for the other parameters. The matrix "matrix_of_priors" is an example matrix of 5 sets of priors


#STEP 3: simulate the IBM with function simu_IBM using the priors generated from step two
list_matrices=f_matrices(grid)
simu_pop=apply(FUN=simu_IBM, X=matrix_of_priors, MARGIN = 1, grid=grid, list_matrices =list_matrices)
#careful with the order of parameters/columns in the prior matrix, it has to correspond to the order of arguments in the simu_IBM function


#STEP 4: compute summary statistics (here example with the "Base" six summary statistics, since we are in the D scenario)

##selecting individuals > 3cm dbh (sizes at the end of the simulation, pedigree and distances between living individuals)
size_list=list()
pedigree_list=list()
distances_list=list()

for (i in 1:length(simu_pop)){
  
  size_list[[i]]=simu_pop[[i]][[2]][simu_pop[[i]][[2]][,Nyear+1]>=3,Nyear+1]
  
  pedigree_list[[i]]=simu_pop[[i]][[1]][simu_pop[[i]][[2]][,Nyear+1]>=3,]
  
  distances_list[[i]]=list_matrices[[1]][pedigree_list[[i]]$id,pedigree_list[[i]]$id]
  if (length(size_list[[i]])>2){diag(distances_list[[i]])<-NA}
  
}


##computing summary statistics
summary_stats_list=list()

for (i in 1:length(simu_pop)){
  
  summary_stats_list[[i]]=f_sumstat_base(size=size_list[[i]], grid=grid, pedigree=pedigree_list[[i]], distances=distances_list[[i]], N=4)
 
}

##dataframe of the summary statistics
summary_stats_matrix=matrix(unlist(summary_stats_list), nrow=length(simu_pop), byrow = T)


#STEP 5: drawing posterior distributions using EasyABC package
library(abc)
reference_summary_stats=c(0.5863213, 21.36013, 55.0136, 0.01752066, 32.37561, -0.3988456)
run_abc=abc(target=reference_summary_stats, param=matrix_of_priors, sumstat=summary_stats_matrix, tol=0.005, method = "loclinear")
posteriors_matrix=run_abc$adj.values


